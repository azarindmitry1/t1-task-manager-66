package ru.t1.dazarin.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.dazarin.tm.model.dto.TaskDto;

import java.util.List;

public interface TestClient {

    static TestClient client(final String baseUrl) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TestClient.class, baseUrl);
    }

    @GetMapping(value = "/findAll")
    List<TaskDto> findAll();

    @GetMapping(value = "/findById/{id}")
    TaskDto findById(@NotNull @PathVariable("id") String id);

    @PostMapping(value = "/create")
    TaskDto create();

    @DeleteMapping(value = "/deleteById/{id}")
    void deleteById(@NotNull @PathVariable("id") String id);

    @DeleteMapping(value = "/deleteAll")
    void deleteAll();

    @PutMapping(value = "/update")
    TaskDto update(@NotNull @RequestBody TaskDto taskDto);

}
