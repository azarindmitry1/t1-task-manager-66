package ru.t1.azarin.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.request.user.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskListByProjectIdRequest(@Nullable final String token) {
        super(token);
    }

}
