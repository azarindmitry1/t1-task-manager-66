package ru.t1.azarin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.azarin.tm.model.Session;

import java.util.List;

@Repository
@Scope("prototype")
public interface SessionRepository extends AbstractUserOwnedRepository<Session> {

    void deleteAllByUserId(@NotNull String userId);

    @Nullable
    List<Session> findAllByUserId(@NotNull String userId);

    @Nullable
    Session findByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

}
