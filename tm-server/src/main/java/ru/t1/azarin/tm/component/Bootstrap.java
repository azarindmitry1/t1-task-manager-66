package ru.t1.azarin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.api.service.IJmsConnectionService;
import ru.t1.azarin.tm.api.service.ILoggerService;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.endpoint.AbstractEndpoint;
import ru.t1.azarin.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Getter
@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private ApplicationContext context;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IJmsConnectionService jmsConnectionService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private AbstractEndpoint[] endpoints;

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPid() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initEndpoints() {
        Arrays.stream(endpoints).forEach(this::registry);
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER IS SHUTTING DOWN **");
    }

    public void run(@Nullable final String[] args) {
        initEndpoints();
        initPid();
        jmsConnectionService.initLogger();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

}