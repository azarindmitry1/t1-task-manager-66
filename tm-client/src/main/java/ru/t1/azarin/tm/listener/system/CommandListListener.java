package ru.t1.azarin.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.api.model.ICommand;
import ru.t1.azarin.tm.event.ConsoleEvent;
import ru.t1.azarin.tm.listener.AbstractListener;

import java.util.Collection;

@Component
public final class CommandListListener extends AbstractSystemListener {

    @NotNull
    public final static String NAME = "commands";

    @NotNull
    public final static String ARGUMENT = "-cmd";

    @NotNull
    public final static String DESCRIPTION = "Display commands of application.";

    @Override
    @EventListener(condition = "@commandListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMMANDS]");
        @NotNull final Collection<AbstractListener> commands = getListeners();
        for (@NotNull final ICommand command : commands) {
            @Nullable final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
